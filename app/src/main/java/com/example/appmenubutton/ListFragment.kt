package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var searchView: SearchView
    private lateinit var listView: ListView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var originalList: List<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedStateInstanceState: Bundle?
    ): View{
        //Agregando variable View
        val View = inflater.inflate(R.layout.fragment_list,container, false)
        listView = View.findViewById(R.id.listAlumno)
        searchView = View.findViewById(R.id.searchView)

        //Lee los alumnos del Array-String y los coloca en la variable items
        val items = resources.getStringArray(R.array.alumnos)
        originalList = items.toList()

        arrayList = java.util.ArrayList()
        arrayList.addAll(items)

        adapter = ArrayAdapter(requireContext(),android.R.layout.simple_list_item_1,arrayList)

        listView.adapter = adapter

        listView.setOnItemClickListener(AdapterView.OnItemClickListener {parent, view, position,id ->
            var alumno:String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage(position.toString() + ":" + alumno)
            builder.setPositiveButton("Ok") { dialog, which ->
                //Accion cuando el usuario hace click en el boton Ok
            }
            builder.show()
        })

        // Configurar el SearchView para filtrar la lista
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val filteredList = if (newText.isNullOrEmpty()) {
                    originalList
                } else {
                    originalList.filter {
                        it.contains(newText, ignoreCase = true)
                    }
                }
                adapter.clear()
                adapter.addAll(filteredList)
                adapter.notifyDataSetChanged()
                return true
            }
        })


        return View
    }
}