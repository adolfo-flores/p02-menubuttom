package com.example.appmenubutton.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class AlumnoDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "sistema.db"
        private const val DATABASE_VERSION = 2
        private const val TEXT_TYPE = "TEXT"
        private const val INTEGER_TYPE = "INTEGER"
        private const val COMMA = ","
        private  val SQL_CREATE_ALUMNO = "CREATE TABLE " +
                "${DefinirTabla.Alumno.TABLA} (" +
                "${DefinirTabla.Alumno.ID} $INTEGER_TYPE PRIMARY KEY AUTOINCREMENT$COMMA " +
                "${DefinirTabla.Alumno.MATRICULA} $TEXT_TYPE$COMMA " +
                "${DefinirTabla.Alumno.NOMBRE} $TEXT_TYPE$COMMA " +
                "${DefinirTabla.Alumno.DOMICILIO} $TEXT_TYPE$COMMA " +
                "${DefinirTabla.Alumno.ESPECIALIDAD} $TEXT_TYPE$COMMA " +
                "${DefinirTabla.Alumno.FOTO} $TEXT_TYPE)"

        private const val SQL_DELETE_ALUMNO = "DROP TABLE IF EXISTS ${DefinirTabla.Alumno.TABLA}"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_ALUMNO)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ALUMNO)
        onCreate(db)
    }
}