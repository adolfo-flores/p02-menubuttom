package com.example.appmenubutton.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf

class dbAlumnos(private val context: Context) {
    private val dbHelper: AlumnoDbHelper = AlumnoDbHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirTabla.Alumno.ID,
        DefinirTabla.Alumno.MATRICULA,
        DefinirTabla.Alumno.NOMBRE,
        DefinirTabla.Alumno.DOMICILIO,
        DefinirTabla.Alumno.ESPECIALIDAD,
        DefinirTabla.Alumno.FOTO
    )

    fun openDataBase() {
        db = dbHelper.writableDatabase
    }

    fun InsertarAlumno(alumno: Alumno): Long {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumno.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumno.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumno.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumno.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumno.FOTO, alumno.foto)
        }
        return db.insert(DefinirTabla.Alumno.TABLA, null, valores)
    }

    fun ActualizarAlumno(alumno: Alumno, id: Int): Int {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumno.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumno.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumno.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumno.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumno.FOTO, alumno.foto)
        }
        return db.update(DefinirTabla.Alumno.TABLA, valores, "${DefinirTabla.Alumno.ID}= ?", arrayOf(id.toString()))
    }

    fun BorrarAlumno(id: Int): Int {
        return db.delete(DefinirTabla.Alumno.TABLA, "${DefinirTabla.Alumno.ID}=?", arrayOf(id.toString()))
    }

    private fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.ID))
            matricula = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.MATRICULA))
            nombre = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.NOMBRE))
            domicilio = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.DOMICILIO))
            especialidad = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.ESPECIALIDAD))
            foto = cursor.getString(cursor.getColumnIndexOrThrow(DefinirTabla.Alumno.FOTO))
        }
    }

    fun getAlumno(matricula: String): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            DefinirTabla.Alumno.TABLA, leerCampos,
            "${DefinirTabla.Alumno.MATRICULA} = ?", arrayOf(matricula),
            null, null, null
        )
        cursor.moveToFirst()
        val alumno = if (cursor.count > 0) mostrarAlumnos(cursor) else Alumno()
        cursor.close()
        return alumno
    }

    fun leerTodos(): ArrayList<Alumno> {
        val cursor = db.query(DefinirTabla.Alumno.TABLA, leerCampos, null, null, null, null, null)
        val listaAlumno = ArrayList<Alumno>()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val alumno = mostrarAlumnos(cursor)
            listaAlumno.add(alumno)
            cursor.moveToNext()
        }
        cursor.close()
        return listaAlumno
    }

    fun close() {
        dbHelper.close()
    }
}
